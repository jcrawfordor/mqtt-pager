from paho.mqtt import client as mqtt_client
import argparse
import logging
import subprocess


def run():
    parser = argparse.ArgumentParser(prog="MQTT-pager", description="MQTT to POCSAG gateway for mmdvm")
    parser.add_argument("host", type=str)
    parser.add_argument("-p", "--port", help="Port number", default=1883, type=int)
    parser.add_argument("-t", "--topic", help="Topic to subscribe to", default="pager", type=str)
    parser.add_argument("-u", "--user", help="MQTT broker username", type=str)
    parser.add_argument("-w", "--password", help="MQTT broker password", type=str)
    parser.add_argument("-c", "--client", help="Client ID string", type=str, default="mmdvm")
    parser.add_argument('-d', '--debug', help="Debug output", action="store_const", dest="loglevel",
                        const=logging.DEBUG, default=logging.WARNING)
    parser.add_argument('-v', '--verbose', help="Verbose output", action="store_const", dest="loglevel",
                        const=logging.INFO)
    args = parser.parse_args()

    logging.basicConfig(level=args.loglevel)
    logging.debug("Debug logging enabled")

    def on_connect(client, userdata, flags, rc):
        client.subscribe(args.topic)
        logging.info(f"Connected to topic {args.topic}")

    # MQTT connection
    client = mqtt_client.Client(args.client)
    if args.user:
        client.username_pw_set(args.user, args.password)
    client.on_connect = on_connect
    client.connect(args.host, args.port)
    logging.info("Connected to MQTT broker")

    def on_message(mclient, userdata, msg):
        logging.info(f"Message: {msg.payload.decode()}")
        try:
            commands = msg.payload.decode().split("|")
            target = commands[0]
            message = commands[1]

            command = ["RemoteCommand", "7642", "page", target, f"{message}"]
            logging.debug(f"Running: {command}")
            subprocess.run(command)

        except Exception:
            logging.exception("An error occurred while processing a message")

    client.on_message = on_message
    logging.info("mqtt-pager is ready")
    client.loop_forever()


if __name__ == "__main__":
    run()
